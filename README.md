# Introducción

El presente manual pretende recoger, al menos de forma esencial, el conocimiento que hemos ido acumulando en Naudit sobre el uso y gestión de Elasticsearch.
La [documentación oficial](https://www.elastic.co/guide/en/elasticsearch/reference/7.11/index.html) y otros muchos recursos (vídeos, artículos de blogs, etc.) siempre serán referencias más completas, pero no buscamos hacer una recopilación extensa del material que hay por ahí.
Con este manual lo que queremos es facilitar el aprendizaje de cómo manejar un software bastante importante en la actividad de nuestra empresa,
además de documentar ciertos temas relacionados con Elasticsearch.

Empezamos este documento en noviembre de 2020 (cuando Elasticsearch estaba como por la versión 7.10-7.11),
y cerramos una primera versión completa en abril de 2021.
Nuestra primera intención es que sirva como base para quien empieza con Elastic, pero está abierto a [contribuciones y mejoras](https://repo1.naudit.es/jartigag/manual-elasticsearch/-/merge_requests).

<center>
    <img src="https://www.naudit.es/wp-content/uploads/logo2.png" width="100%">
</center>

#### Lista de contribuidores

- [Javier Artiga](mailto:javier.artiga@naudit.es)
- [Pablo Guembe](mailto:pablo.guembe@naudit.es)
- [David Moreno](mailto:david.moreno@naudit.es)
