# Summary

* [Introducción](README.md)
* [Conceptos básicos](conceptos_basicos/README.md)
   * [API de Elasticsearch](conceptos_basicos/api_de_elasticsearch.md)
   * [Templates](conceptos_basicos/templates.md)
   * [Queries](conceptos_basicos/queries.md)
* [Librerías de Elasticsearch en Python](librerias_de_elasticsearch_en_python/README.md)
   * [elasticsearch](librerias_de_elasticsearch_en_python/elasticsearch.md)
   * [elasticsearch_dsl](librerias_de_elasticsearch_en_python/elasticsearch_dsl.md)
   * [requests](librerias_de_elasticsearch_en_python/requests.md)
* [Cómo funciona Elasticsearch](como_funciona_elasticsearch/README.md)
   * [Java Virtual Machine](como_funciona_elasticsearch/java_virtual_machine.md)
   * [Lucene](como_funciona_elasticsearch/lucene.md)
   * [Análisis cuantitativo de rendimiento de un cluster](como_funciona_elasticsearch/analisis_cuantitativo_cluster.md)
   * [Configuración de Elasticsearch](como_funciona_elasticsearch/configuracion_de_elasticsearch.md)
   * [Monitorización de Elasticsearch](como_funciona_elasticsearch/monitorizacion_de_elasticsearch.md)
* [Por qué Elasticsearch](por_que_elasticsearch/README.md)
   * [Investigación para "Loginson"](por_que_elasticsearch/investigacion_para_loginson.md)

